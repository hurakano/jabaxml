package bookStore;

import org.simpleframework.xml.*;

@Root(name="author")
public class Author
{
	@Attribute
	private String id;
	@Element
	private String name;
	@Element
	private String birthDate;
	
/////////////////////////////////////////////////

	public Author(){}

	public Author(String id, String name, String date)
	{
		this.id = id;
		this.name = name;
		this.birthDate = date;
	}
//////////////////////////////////////////////////

	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
////////////////////////////////////////////////

	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
/////////////////////////////////////////////////

	public String getBirthDate()
	{
		return birthDate;
	}
	public void setBirthDate(String date)
	{
		this.birthDate = date;
	}
}
