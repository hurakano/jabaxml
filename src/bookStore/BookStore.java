package bookStore;

import java.util.ArrayList;
import org.simpleframework.xml.*;

@Root(name="bookStore")
public class BookStore
{
	@ElementList
	private ArrayList<Author> authors;
	@ElementList
	private ArrayList<Book> books;
	
/////////////////////////////////////////////

	public BookStore()
	{
		authors = new ArrayList<>();
		books = new ArrayList<>();
	}
///////////////////////////////////////////

	public void addAuthor(Author obj)
	{
		authors.add(obj);
	}
/////////////////////////////////////////

	public void addBook(Book obj)
	{
		books.add(obj);
	}
/////////////////////////////////////////

	public ArrayList<Author> getAuthors()
	{
		return authors;
	}
///////////////////////////////////////////

	public ArrayList<Book> getBooks()
	{
		return books;
	}
}
