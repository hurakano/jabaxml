package bookStore;

import org.simpleframework.xml.*;

@Root(name="book")
public class Book
{
	@Attribute(name="author_id")
	private String authorId;
	@Element
	private String title;
	@Element
	private String releaseDate;
	@Element
	private String genere;
	@Element
	private int quantity;
	@Element
	private double price;
	
///////////////////////////////////////////////

	public Book(){}

	public Book(String author, String title, String date, String genere, int quantity, double price)
	{
		this.authorId = author;
		this.title = title;
		this.releaseDate = date;
		this.genere = genere;
		this.quantity = quantity;
		this.price = price;
	}
	
///////////////////////////////////////////////

	public String getAuthorId()
	{
		return authorId;
	}
	public void setAuthorId(String author)
	{
		this.authorId = author;
	}
//////////////////////////////////////////////

	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
////////////////////////////////////////////

	public String getReleaseDate()
	{
		return releaseDate;
	}
	public void setReleaseDate(String date)
	{
		this.releaseDate = date;
	}
/////////////////////////////////////////////////

	public String getGenere()
	{
		return genere;
	}
	public void setGenere(String genere)
	{
		this.genere = genere;
	}
//////////////////////////////////////////////

	public int getQuantity()
	{
		return quantity;
	}
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
////////////////////////////////////////////////////////

	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price = price;
	}
}
