package bookStore;

import java.lang.System;
import java.io.File;
import java.awt.EventQueue;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.Format;
import org.simpleframework.xml.*;
import java.awt.event.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.CardLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.*;

class Store
{
	private BookStore store;
	
	private JFrame mainFrame;
	private JPanel mainPanel;
	private JPanel storePanel;
	
	private JPanel authorModPanel;
	private JTextField authorNameField;
	private JTextField authorDateField;
	
	private JPanel bookModPanel;
	private JComboBox bookAthorCombo;
	private JTextField bookTitleField;
	private JTextField bookDateField;
	private JTextField bookGenereField;
	private JSpinner bookQuantitySpinner;
	private JSpinner bookPriceSpinner;
	
	
	private JButton addAuthorButton;
	private JButton okButton1;
	private JButton saveButton1;
	private JButton deleteButton1;
	
	private JButton addBookButton;
	private JButton okButton2;
	private JButton saveButton2;
	private JButton deleteButton2;
	
	private JTabbedPane tabbedPanel;
	private JPanel authorPanel;
	private JPanel bookPanel;
	private JList authorList;
	private JList bookList;
	
	int activeIndex = -1;
////////////////////////////////////////////////

	public void runStore()
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					initialize();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
/////////////////////////////////////////////
	
	
	public void initialize()
	{
		store = new BookStore();
	
		mainFrame = new JFrame();
		mainFrame.setBounds(100, 100, 300, 500);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		mainPanel = new JPanel(new CardLayout());
		
		storePanel = new JPanel(null);
		tabbedPanel = new JTabbedPane();
		tabbedPanel.setBounds(10, 10, 280, 420);
		
		authorPanel = new JPanel();
		authorPanel.setLayout(null);
		bookPanel = new JPanel();
		bookPanel.setLayout(null);
		
		authorList = new JList();
		authorList.setBounds(10, 10, 250, 340);
		authorList.addListSelectionListener(new ListSelectionListener()
		{
			public void valueChanged(ListSelectionEvent arg0)
			{
				int index = authorList.getSelectedIndex();
				if(index != -1)
					showAuthor(index);
			}
		});
		
		bookList = new JList();
		bookList.setBounds(10, 10, 250, 340);
		bookList.addListSelectionListener(new ListSelectionListener()
		{
			public void valueChanged(ListSelectionEvent arg0)
			{
				int index = bookList.getSelectedIndex();
				if(index != -1)
					showBook(index);
			}
		});
		
		authorPanel.add(authorList);
		addAuthorButton = new JButton("add author");
		addAuthorButton.setBounds(10, 360, 150, 30);
		addAuthorButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				addAuthor();
			}
		});
		authorPanel.add(addAuthorButton);
		
		bookPanel.add(bookList);
		addBookButton = new JButton("add book");
		addBookButton.setBounds(10, 360, 150, 30);
		addBookButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				addBook();
			}
		});
		bookPanel.add(addBookButton);
		
		tabbedPanel.add("Authors", authorPanel);
		tabbedPanel.add("Books", bookPanel);
		
		storePanel.add(tabbedPanel);
		
		/////////////////////////////////////////////////
		
		okButton1 = new JButton("OK");
		okButton1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				activeIndex = -1;
				updateLists();
				CardLayout layout = (CardLayout)(mainPanel.getLayout());
				layout.show(mainPanel, "storePanel");
			}
		});
		saveButton1 = new JButton("Save");
		saveButton1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				saveAuthor();
			}
		});
		deleteButton1 = new JButton("Delete");
		deleteButton1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				deleteAuthor();
			}
		});
		
		okButton2 = new JButton("OK");
		okButton2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				activeIndex = -1;
				updateLists();
				CardLayout layout = (CardLayout)(mainPanel.getLayout());
				layout.show(mainPanel, "storePanel");
			}
		});
		saveButton2 = new JButton("Save");
		saveButton2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				saveBook();
			}
		});
		deleteButton2 = new JButton("Delete");
		deleteButton2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				deleteBook();
			}
		});
		
		authorModPanel = new JPanel(null);
		authorNameField = new JTextField();
		authorNameField.setBounds(10, 10, 270, 30);
		authorDateField = new JTextField();
		authorDateField.setBounds(10, 50, 270, 30);
		okButton1.setBounds(10, 90, 70, 30);
		saveButton1.setBounds(90, 90, 70, 30);
		deleteButton1.setBounds(170, 90, 100, 30);
		authorModPanel.add(authorNameField);
		authorModPanel.add(authorDateField);
		authorModPanel.add(okButton1);
		authorModPanel.add(saveButton1);
		authorModPanel.add(deleteButton1);
		
		bookModPanel = new JPanel(null);
		bookAthorCombo = new JComboBox();
		bookAthorCombo.setBounds(10, 10, 270, 30);
		bookTitleField = new JTextField();
		bookTitleField.setBounds(10, 50, 270, 30);
		bookDateField = new JTextField();
		bookDateField.setBounds(10, 90, 270, 30);
		bookGenereField = new JTextField();
		bookGenereField.setBounds(10, 130, 270, 30);
		bookQuantitySpinner = new JSpinner(new SpinnerNumberModel(0, 0, 999, 1));
		bookQuantitySpinner.setBounds(10, 170, 270, 30);
		bookPriceSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 999, 0.01));
		bookPriceSpinner.setBounds(10, 210, 270, 30);
		okButton2.setBounds(10, 250, 70, 30);
		saveButton2.setBounds(90, 250, 70, 30);
		deleteButton2.setBounds(170, 250, 100, 30);
		bookModPanel.add(bookAthorCombo);
		bookModPanel.add(bookTitleField);
		bookModPanel.add(bookDateField);
		bookModPanel.add(bookGenereField);
		bookModPanel.add(bookQuantitySpinner);
		bookModPanel.add(bookPriceSpinner);
		bookModPanel.add(okButton2);
		bookModPanel.add(saveButton2);
		bookModPanel.add(deleteButton2);
		
		JMenuBar menuBar = new JMenuBar();
		mainFrame.setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenuItem saveItem = new JMenuItem("Save");
		saveItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				JFileChooser fileChooser = new JFileChooser();
				int status = fileChooser.showOpenDialog(mainFrame);
				if(status == JFileChooser.APPROVE_OPTION)
				{
					save(fileChooser.getSelectedFile());
				}
			}
		});
		
		JMenuItem openItem = new JMenuItem("Open");
		openItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				JFileChooser fileChooser = new JFileChooser();
				int status = fileChooser.showOpenDialog(mainFrame);
				if(status == JFileChooser.APPROVE_OPTION)
				{
					load(fileChooser.getSelectedFile());
					updateLists();
				}
			}
		});
		
		fileMenu.add(saveItem);
		fileMenu.add(openItem);
		
		
		mainPanel.add(storePanel, "storePanel");
		mainPanel.add(authorModPanel, "authorModPanel");
		mainPanel.add(bookModPanel, "bookModPanel");
		
		mainFrame.add(mainPanel);
		mainFrame.setVisible(true);
		load(new File("bookStore.xml"));
		updateLists();
	}
//////////////////////////////////////////////////////////////

	public void updateLists()
	{
		DefaultListModel<String> authorListModel = new DefaultListModel<>();
		DefaultListModel<String> bookListModel = new DefaultListModel<>();
		
		for(Author x: store.getAuthors())
			authorListModel.addElement(x.getName());
			
		for(Book x: store.getBooks())
			bookListModel.addElement(x.getTitle());
			
		authorList.setModel(authorListModel);
		bookList.setModel(bookListModel);
	}
///////////////////////////////////////////////////////////////

	public void addAuthor()
	{
		store.addAuthor(new Author("newId", "Name", "birth date"));
	
		showAuthor(store.getAuthors().size()-1);
	}
////////////////////////////////////////////////////////////////

	public void showAuthor(int index)
	{
		activeIndex = index;
		Author author = store.getAuthors().get(index);
		
		authorNameField.setText(author.getName());
		authorDateField.setText(author.getBirthDate());
		
		CardLayout layout = (CardLayout)(mainPanel.getLayout());
		layout.show(mainPanel, "authorModPanel");
	}	
///////////////////////////////////////////////////////////////

	public void saveAuthor()
	{
		String id, name, date;
		
		name = authorNameField.getText();
		date = authorDateField.getText();
		
		if(name.equals("Name"))
		{
			JOptionPane.showMessageDialog(mainFrame, "Please enter valid author", "Name error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		String nameList[] = name.split(" ");
		id = new String("");
		for(int i = 0; i < nameList.length-1; i++)
			id += nameList[i].toUpperCase().charAt(0) + ".";
			
		id += nameList[nameList.length-1];
		
		for(int i = 0; i < store.getAuthors().size(); i++)
		{
			if(i != activeIndex && store.getAuthors().get(i).getId().equals(id))
			{
				JOptionPane.showMessageDialog(mainFrame, "Id already exists", "Name error", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		
		store.getAuthors().get(activeIndex).setId(id);
		store.getAuthors().get(activeIndex).setName(name);
		store.getAuthors().get(activeIndex).setBirthDate(date);
		
		activeIndex = -1;
		CardLayout layout = (CardLayout)(mainPanel.getLayout());
		updateLists();
		layout.show(mainPanel, "storePanel");
	}
//////////////////////////////////////////////////////////////

	public void deleteAuthor()
	{
		for(Book book: store.getBooks())
			if(book.getAuthorId().equals(store.getAuthors().get(activeIndex).getId()))
			{
				JOptionPane.showMessageDialog(mainFrame, "This author is assigned to existing book", "Id in use", JOptionPane.ERROR_MESSAGE);
				return;
			}
	
		store.getAuthors().remove(activeIndex);
		activeIndex = -1;
		CardLayout layout = (CardLayout)(mainPanel.getLayout());
		updateLists();
		layout.show(mainPanel, "storePanel");
	}

/////////////////////////////////////////////////////////////
	public void addBook()
	{
		if(store.getAuthors().size() == 0)
		{
			JOptionPane.showMessageDialog(mainFrame, "Plese add author first", "No existing authors", JOptionPane.ERROR_MESSAGE);
				return;
		}
		store.addBook(new Book(store.getAuthors().get(0).getId(), "Title", "release date", "Genere", 0, 0));
		
		showBook(store.getBooks().size()-1);
	}
///////////////////////////////////////////////////////////////////

	public void showBook(int index)
	{
		activeIndex = index;
		
		String authorIds[] = new String[store.getAuthors().size()];
		for(int i = 0; i < store.getAuthors().size(); i++)
			authorIds[i] = store.getAuthors().get(i).getId();
			
		bookAthorCombo.setModel(new DefaultComboBoxModel(authorIds));
		bookAthorCombo.setSelectedItem(store.getBooks().get(activeIndex).getAuthorId());
		
		bookTitleField.setText(store.getBooks().get(activeIndex).getTitle());
		bookDateField.setText(store.getBooks().get(activeIndex).getReleaseDate());
		bookGenereField.setText(store.getBooks().get(activeIndex).getGenere());
		bookQuantitySpinner.setValue(store.getBooks().get(activeIndex).getQuantity());
		bookPriceSpinner.setValue(store.getBooks().get(activeIndex).getPrice());
		
		CardLayout layout = (CardLayout)(mainPanel.getLayout());
		layout.show(mainPanel, "bookModPanel");
	}
////////////////////////////////////////////////////////////////

	public void saveBook()
	{
		String authorId, title, date, genere;
		int quantity;
		double price;
		
		authorId = (String)bookAthorCombo.getSelectedItem();
		title = bookTitleField.getText();
		date = bookDateField.getText();
		genere = bookGenereField.getText();
		quantity = (int)bookQuantitySpinner.getValue();
		price = (double)bookPriceSpinner.getValue();
		
		store.getBooks().get(activeIndex).setAuthorId(authorId);
		store.getBooks().get(activeIndex).setTitle(title);
		store.getBooks().get(activeIndex).setReleaseDate(date);
		store.getBooks().get(activeIndex).setGenere(genere);
		store.getBooks().get(activeIndex).setQuantity(quantity);
		store.getBooks().get(activeIndex).setPrice(price);
		
		activeIndex = -1;
		CardLayout layout = (CardLayout)(mainPanel.getLayout());
		updateLists();
		layout.show(mainPanel, "storePanel");
	}
//////////////////////////////////////////////////////////////

	public void deleteBook()
	{
		store.getBooks().remove(activeIndex);
		activeIndex = -1;
		CardLayout layout = (CardLayout)(mainPanel.getLayout());
		updateLists();
		layout.show(mainPanel, "storePanel");
	}
/////////////////////////////////////////////////////////////////

	public void save(File file)
	{
		try
		{
			Serializer ser = new Persister(new Format("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<?xml-stylesheet type=\"text/xsl\" href=\"bookStore.xsl\"?>"));
			
			ser.write(store, file);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
/////////////////////////////////////////////////////////

	public void load(File file)
	{
		try
		{
			Serializer ser = new Persister();
			
			store = ser.read(BookStore.class, file);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
