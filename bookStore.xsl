<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/1999/xhtml">

<xsl:output omit-xml-declaration="yes" indent="yes" method="xml" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"/>
     
<xsl:template match="/">
<html lang="pl">
	<head>
		<title>Book store</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style type="text/css">
			table, td, th, tr {
				border: 2px solid black;
				border-collapse: collapse;
				padding: 4px;
			}
			tr td:last-child {
				text-align: right;
			}
		</style>
	</head>
	<body>
		<h1>Autorzy</h1>
		<table>
			<tr><th>Imię i nazwisko</th><th>data urodzenia</th></tr>
			
			<xsl:for-each select="bookStore/authors/author">
				<tr>
					<td><xsl:value-of select="name"/></td>
					<td><xsl:value-of select="birthDate"/></td>
				</tr>
			</xsl:for-each>
		</table>
		
		<h1>Książki</h1>
		<table>
			<tr><th>Autor</th><th>Tytuł</th><th>Cena</th><th>Ilość w <br/>magazynie</th></tr>
			
			<xsl:for-each select="bookStore/books/book">
				<tr>
					<td><xsl:value-of select="@author_id"/></td>
					<td><xsl:value-of select="title"/></td>
					<td><xsl:value-of select="price"/>zł</td>
					<td><xsl:value-of select="quantity"/></td>
				</tr>
			</xsl:for-each>
		</table>
	</body>
</html>
</xsl:template>

</xsl:stylesheet>
